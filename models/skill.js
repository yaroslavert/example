module.exports = function (sequelize, DataTypes) {
    let skill = sequelize.define('skill', {
        title: {
            type: DataTypes.STRING,
            allowNull: false
        }   
    }, {
        freezeTableName: true
    });
    skill.associate = function(models) {
        skill.hasMany(models.skills);
    }

    return skill;
};