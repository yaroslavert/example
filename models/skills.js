module.exports = function (sequelize, DataTypes) {
    let skills = sequelize.define('skills', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        persent: {
            type: DataTypes.DOUBLE,
            allowNull: false
        }
    }, {
        freezeTableName: true
    });
    skills.associate = function(models) {

    }

    return skills;
};