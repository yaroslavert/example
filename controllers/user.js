const express = require('express');
const router = express.Router();
const db = require('../models');
const config = require('config');

module.exports = function (app) {
    app.use('/', router);
};

router.route('/skills')
    .get((req, res, next) => {
        db.skill.findAll({
            include: [db.skills]
        }).then((data) => {
            res.send(data);
        }).catch((err) => {
            console.log('err', err);
            res.status(400).send(null);
        })
    }).post((req, res, next) => {
        db.skill.create(req.body)
            .then((data) => {
                res.send(data);
            }).catch((err) => {
                console.log('err', err);
                res.status(400).send(null);
            })
    });


router.route('/skills/:id')
    .get((req, res, next) => {
        db.skill.findAll({})
            .then((data) => {
                res.send(data);
            }).catch((err) => {
                console.log('err', err);
                res.status(400).send(null);
            })
    }).post((req, res, next) => {
        db.skills.create(Object.assign({}, req.body, {skillId: req.params.id}))
            .then((data) => {
                res.send(data);
            }).catch((err) => {
                console.log('err', err);
                res.status(400).send(null);
            })
    });
