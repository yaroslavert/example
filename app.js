const path = require('path');
const fs = require('fs');
const express = require('express');
const config = require('config');
const bodyParser = require('body-parser');
const app = express();

const db = require('./models');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const controllers = fs.readdirSync(path.join(config.get('root'), '/controllers')).sort();
controllers.forEach(controller => require(`./controllers/${controller}`)(app));

app.get('/', (req, res) => {
    res.send('work');
});
db.sequelize
    .sync({force: true})
    .then(() => {
        if (module.parent) throw new Error('module will not parent');;
        app.listen(config.get('port'), function () {
            console.log('Express server listening on port ' + config.get('port'));
        });
    }).catch(function (e) {
        throw new Error(e);
    });